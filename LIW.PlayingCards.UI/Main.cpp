
// Playing Cards
// Logan Weyers

#include <iostream>
#include <conio.h>
#include <string>


using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };

enum Suit { Heart, Diamond, Spade, Club }; 

struct Card {
	Rank Rank;
	Suit Suit;
};

int main()
{


	(void)_getch();
	return 0;
}
